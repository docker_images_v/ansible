FROM python:3-slim-bullseye

RUN apt-get update &&\
    apt-get clean &&\
    pip3 install ansible &&\
    pip3 cache purge &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* &&\
    export ANSIBLE_HOST_KEY_CHECKING=False
